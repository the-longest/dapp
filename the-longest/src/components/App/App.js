import React, { Component } from 'react'
import { ethers } from 'ethers'
import './App.css'

import Line from './../Line/Line';
import TheLongest from './../TheLongest/TheLongest';
// import TopTen from './../TopTen/TopTen';

import theLongestTokenFile from './../../TheLongest.json';
const theLongestTokenAddress = '0x61492D9582600bc07eB88480D2211017B431bC07'
class App extends Component {
  componentDidMount() {
    this.loadState()
  }

  async loadState() {

    const provider = new ethers.providers.Web3Provider(window.ethereum)
    const signer = await provider.getSigner()
    const signerAddress = await signer.getAddress()

    const theLongestToken = new ethers.Contract(theLongestTokenAddress, theLongestTokenFile.abi, provider)

    const theLongestTokenBalance = await theLongestToken.balanceOf(signerAddress)
    let theLongestTokenBalanceIndices = []
    for (let i = 0; i < theLongestTokenBalance; i++) {
      const id = await theLongestToken.tokenOfOwnerByIndex(signerAddress, i)
      theLongestTokenBalanceIndices.push(id.toString())
    }

    this.setState({
      wallet: {
        address: signerAddress,
        balance: ethers.utils.formatEther(await signer.getBalance()).toString(),
      },
      theLongestToken: {
        address: theLongestToken.address,
        abi: theLongestToken.interface.format(ethers.utils.FormatTypes.json),
        balance: ethers.utils.formatUnits(theLongestTokenBalance, 0).toString(),
        owned: theLongestTokenBalanceIndices,
      }
    })
  }

  constructor(props) {
    super(props)
    this.state = { wallet: '', theLongestToken: { address: '', abi: [], balance: '', owned: [] } };
  }

  render() {
    return (
      <div className="container">
        <h1>The longest</h1>
        <p>Your account: {this.state.wallet.address}</p>
        <p>Your ETH balance: {this.state.wallet.balance}</p>
        <p>TheLongestToken address: {this.state.theLongestToken.address}</p>
        <p>TheLongestToken balance: {this.state.theLongestToken.balance}</p>
        {/* <p> abi: {this.state.theLongestToken.abi}</p> */}
        <p/>
        <TheLongest address={theLongestTokenAddress} abi={theLongestTokenFile.abi} />
        {/* <TopTen address={theLongestTokenAddress} abi={theLongestTokenFile.abi}/> */}
        <h2>Your line</h2>
        {this.state.theLongestToken.owned.map(id =>
          <p>
            <Line address={theLongestTokenAddress} abi={theLongestTokenFile.abi} id={id} />
          </p>
        )}
      </div>
    );
  }
}

export default App;
