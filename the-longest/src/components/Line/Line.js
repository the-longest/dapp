import React, { Component } from 'react'
import { ethers } from 'ethers'
import './Line.css'

class Line extends Component {
  componentDidMount() {
    this.loadState(this.props.address, this.props.abi)
  }

  async loadState(address, abi) {
    const id = this.state.id

    const provider = new ethers.providers.Web3Provider(window.ethereum)
    const theLongestToken = new ethers.Contract(address, abi, provider)

    const uri = await theLongestToken.tokenURI(id)
    this.setState({
      id: id,
      uri: uri
    })
  }

  constructor(props) {
    super(props)
    this.state = { id: props.id }
  }

  render() {
    if (this.state.uri !== undefined) {
      return (
        <div className="container">
          <Picture uri={this.state.uri} />
        </div>
      );
    } else {
      return (
        <div className="container">
        </div>
      );
    }
  }
}

function Picture(props) {
  const uri = props.uri
  const uri_json = uri.replace('data:application/json;base64,', '')
  const base64ToString = Buffer.from(uri_json, 'base64').toString('ascii').replaceAll("'", '"')
  const json = JSON.parse(base64ToString)
  const name = json.name
  const description = json.description
  const image = json.image
  const attributes = json.attributes
  return (
    <div>
      name: {name}<br />
      description: {description}<br />
      {/* image: {image}<br /> */}
      attributes: {JSON.stringify(attributes)}<br />
      <img
        src={image}
        width="500"
        height="500"
      />
    </div>
  )
}

export default Line;
