import React, { Component } from 'react'
import { ethers } from 'ethers'
import './TheLongest.css'

class TheLongest extends Component {
  componentDidMount() {
    this.loadState(this.props.address, this.props.abi)
  }
  async loadState(address, abi) {
    if (address === undefined || abi === undefined) {
      console.warn('TheLongest: address or abi is undefined')
    }
    const provider = new ethers.providers.Web3Provider(window.ethereum)
    const theLongestToken = new ethers.Contract(address, abi, provider)

    const theLongest = await theLongestToken.theLongest()
    const theLongestLength = await theLongestToken.theLongestLength()

    this.setState({
      theLongest: theLongest.toString(),
      theLongestLength: theLongestLength.toString()
    })
  }

  constructor(props) {
    super(props)
    this.state = { }
  }

  render() {
    return (
      <div className="container">
        <h2>The Longest</h2>
        The longest has {this.state.theLongest} with a length of {this.state.theLongestLength}.
      </div>
    );
  }

}

export default TheLongest;
