import React, { Component } from 'react'
import { ethers } from 'ethers'
import './TopTen.css'

class TopTen extends Component {
  componentDidMount() {
    this.loadState(this.props.address, this.props.abi)
  }

  async loadState(address, abi) {
    if (address === undefined || abi === undefined) {
      console.warn('TopTen: address or abi is undefined')
    }
    const provider = new ethers.providers.Web3Provider(window.ethereum)
    const theLongestToken = new ethers.Contract(address, abi, provider)
    const topTenAddresses = await theLongestToken.getTopTen()

    let topTenPromises = topTenAddresses.map((address) => {
      if (address === ethers.constants.AddressZero) {
      return new Promise((resolve, reject) => {
        resolve({
          address: address,
          length: '0',
        });
      });
      } else {
        return theLongestToken.tokenOfOwnerByIndex(address, 0).then(tokenID => {
          return theLongestToken.getLength(tokenID).then(length => {
            return {
              address: address,
              length: length.toString(),
            }
          })
        })
      }
    })

    let topTen = await Promise.all(topTenPromises);
    this.setState({
      topTen: topTen
    })
  }

  constructor(props) {
    super(props)
    this.state = { topTen: [] }
  }

  render() {
    return (
      <div className="container">
        <h2>Top Ten</h2>
        <ol>
        {this.state.topTen.map((wallet, index) =>
            <li key={index}>{wallet.address} with {wallet.length}</li>
        )}
        </ol>
      </div>
    );
  }

}

export default TopTen;
